function validate() 
{
    let your_name = document.getElementById("fname");
    let your_surname = document.getElementById("lname");
    let email = document.getElementById("email");
    let password = document.getElementById("pass");
    var check = 0;
    if (isEmpty(your_name.value)) 
    {
        check = check + 1;
        document.getElementById("fnameText").innerHTML = 'First Name cannot be empty';
        document.getElementById("fnameText").style.color = 'red';
        document.getElementById("fname").style = 'border: 1px solid red;';

    }
    if (isEmpty(your_surname.value)) 
    {
        check = check + 1;
        document.getElementById("lnameText").innerHTML = 'Last Name cannot be empty';
        document.getElementById("lnameText").style.color = 'red';
        document.getElementById("lname").style = 'border: 1px solid red;';
    }
    if (isEmpty(email.value)) 
    {
        check = check + 1;
        document.getElementById("emailText").innerHTML = 'Email id cannot be empty';
        document.getElementById("emailText").style.color = 'red';
        document.getElementById("email").style = 'border: 1px solid red;';   
    }
    if(!validEmail(email.value))
    {
        check = check + 1;
        document.getElementById("emailText").innerHTML = "Email id is not correct;"
        document.getElementById("emailText").style.color = 'red';
        document.getElementById("email").style = 'border: 1px solid red;';
    }
    if (isEmpty(password.value)) 
    {
        check = check + 1;
        document.getElementById("passText").innerHTML = 'Password cannot be empty';
        document.getElementById("passText").style.color = 'red';
        document.getElementById("pass").style = 'border: 1px solid red;';
        

    } 
    if(check==0)
    {
        document.getElementById("fname").style = 'border: 1px solid green;';
        document.getElementById("lname").style = 'border: 1px solid green;';
        document.getElementById("email").style = 'border: 1px solid green;';
        document.getElementById("pass").style = 'border: 1px solid green;';
        document.getElementById("fnameText").innerHTML = "";
        document.getElementById("lnameText").innerHTML = "";
        document.getElementById("emailText").innerHTML = "";
        document.getElementById("passText").innerHTML = "";
        alert("Hello " + your_name.value + " " + your_surname.value);
        
    }

}

function isEmpty(value) {
    return value.length == 0;
}

function validEmail(email) {
    const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailRegex.test(email);
}

// $("#login_form").submit(function(e) {
//     e.preventDefault();
// });